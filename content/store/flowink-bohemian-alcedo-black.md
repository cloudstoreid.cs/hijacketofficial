---
title: Flowink Bohemian Alcedo Black - BM-ALCEDO
description: Jual jaket Flowink Bohemian Alcedo Black - BM-ALCEDO
date: '2018-07-05T17:48:14+07:00'
slug: bm-alcedo
model:
  - bohemian
brand:
  - flowink
thumbnail: /images/bohemian/bohemian-alcedo-black.jpg
image:
  - /images/bohemian/bohemian-alcedo-black-1.jpg
  - /images/bohemian/bohemian-alcedo-black-2.jpg
  - /images/bohemian/bohemian-alcedo-black-3.jpg
  - /images/bohemian/bohemian-alcedo-black-4.jpg
sku: BM-ALCEDO
badge: ''
berat: 700 gram
layout: flowink
color:
  - Black
size:
  - name: All Size
    price: 210000
stock: true
---

FLOWINK BOHEMIAN ORIGINAL Series dirancang khusus buat kamu yang mau mengekspresikan gaya hidup bebas dan pecinta seni artistik

Flowink Bohemian BlueJay memberikan gaya ethnic untuk harimu dan menjadi sorotan publik

- • Fabric: Body: 80% cotton/20% polyester. Hand : 100% polyester, Hood : 50% cotton/50% polyester.
- • Machine wash
- • Shown: Black
- • Printed Art
- • Machine Wash

#### Tabel Ukuran Jaket Flowink Original


| Ukuran          | All Size        | XL              |
|:--------------- |:---------------:|:---------------:|
| Lingkar Dada    | 108-110         | 112-114	      |
| Lingkar Lengan  | 46-48           | 48-50  	      |
| Panjang Tangan  | 60-62           | 62-64  	      |
| Panjang Badan   | 67-69           | 70-72  	      |
