---
title: Hijacket Yukata Brown - HJ-YK
description: Jual jaket muslimah Hijacket Yukata Brown - HJ-YK
date: '2018-04-04T17:48:14+07:00'
slug: hj-yk-brown
product:
  - yukata
brand:
  - hijacket
thumbnail: /images/yukata/yk-brown.jpg
image:
  - /images/yukata/yk-brown-1.jpg
  - /images/yukata/yk-brown-2.jpg
  - /images/yukata/yk-brown-3.jpg
  - /images/yukata/yk-brown-4.jpg
  - /images/yukata/yk-brown-5.jpg
  - /images/yukata/yk-brown-6.jpg
sku: HJ-YK-BROWN
badge: ''
berat: 730 gram
color:
  - Brown
size:
  - name: All Size
    price: 205000
  - name: XL
    price: 215000
stock: true
---

Hijacket Yukata Original seri hijacket terbaru dengan tema Sporty model Japan dengan style Finger dipadu dengan sablon Hijaber berkualitas dibagian saku depan menjadikan hijacket yukata ini merupakan identitas dan jati diri seorang hijaber sejati disempurnakan dengan perpaduan warna elegan dengan bahan premium fleece yang tebal, nyaman dan adem.

- ▶ Ukuran : ALL SIZE FIT TO L hingga XL (XL Nambah 10.000)

- ▶ Material : Premium Fleece yang “SOFT TOUCH” langsung dari pabrik pengolah kain berpengalaman

- ▶ Proses : Dibuat Handmade dengan penjahit terbaik yang berpengalaman lebih dari 5 tahun

- ▶ Sablonan Berkualitas

- ▶ Bukan sekedar fashion. Namun menguatkan “JATI DIRI / IDENTITAS” Hijaber yang modis dan stylish

- ▶ Foto & Video : 100% sama dengan hijacket yang diterima karena kami foto & video model sendiri.

Ada 4 variasi warna Hijacket Yukata Original, pilih style favorit ukhti

#### Tabel Ukuran Hijacket Yukata Original


| Ukuran          | All Size        | XL              |
|:--------------- |:---------------:|:---------------:|
| Lingkar Dada    | 101-102         | 108-110	      |
| Lingkar Lengan  | 40-42           | 43-45  	      |
| Panjang Tangan  | 55-57           | 55-57  	      |
| Panjang Badan   | 93-95           | 93-95  	      |
