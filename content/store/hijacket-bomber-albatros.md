---
title: Hijacket Bomber Albatros - HJ-BMB
description: Jual jaket muslimah Hijacket Bomber Albatros - HJ-BMB
date: '2018-04-04T17:48:14+07:00'
slug: hj-bmb-albatros
product:
  - bomber
brand:
  - hijacket
thumbnail: /images/bomber/bmb-albatros.jpg
image:
  - /images/bomber/bmb-albatros-1.jpg
  - /images/bomber/bmb-albatros-2.jpg
  - /images/bomber/bmb-albatros-3.jpg
  - /images/bomber/bmb-albatros-4.jpg
  - /images/bomber/bmb-albatros-5.jpg
  - /images/bomber/bmb-albatros-6.jpg
sku: HJ-BMB-ALBATROS
badge: ''
berat: 700 gram
color:
  - Albatros
size:
  - name: All Size
    price: 215000
  - name: XL
    price: 225000
stock: true
---

HIJACKET BOMBER ORIGINAL dirancang khusus untuk sebagai Icon Powerful& Strong Hijaber. Dengan model pilot bomber, dipadu dengan tali kerut agar bias tampil stylish saat disletting, dan tetap keren saat tidak disletting. Dengan dalaman Quilting Dourmill Dacron yang menambah Lux seri HJ Bomber ini

- ▶️ Ukuran : ALL SIZE FIT TO L hingga XL (XL NAMBAH 10.000 dari harga all size L)

- ▶️ Material Luar : PREMIUM Scout Puma “Waterproof”

- ▶️ Material Dalam : Full Quilting Dourmill Dacron

- ▶️ Proses : Dibuat Handmade dengan penjahit terbaik yang berpengalaman lebih dari 5 tahun

- ▶️ Bukan sekedar fashion. Namun menguatkan “JATI DIRI / IDENTITAS” Hijaber yang modis dan stylish

- ▶️ Foto & Video : 100% sama dengan hijacket yang diterima karena kami foto & video model sendiri.

Ada 4 variasi warna Hijacket Bomber Original

#### Tabel Ukuran Hijacket Bomber Original


| Ukuran          | All Size        | XL              |
|:--------------- |:---------------:|:---------------:|
| Lingkar Dada    | 106-108         | 112-114	      |
| Lingkar Lengan  | 46-48           | 46-48  	      |
| Panjang Tangan  | 55-57           | 55-57  	      |
| Panjang Badan   | 78-80           | 78-80  	      |
