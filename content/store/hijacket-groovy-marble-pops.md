---
title: Hijacket Groovy Marble Pops - HJ-GR
description: Jual jaket muslimah Hijacket Groovy Marble Pops - HJ-GR
date: '2018-04-04T17:48:14+07:00'
slug: hj-gr-marble-pops
product:
  - groovy
brand:
  - hijacket
thumbnail: /images/groovy/marble-pops.jpg
image:
  - /images/groovy/marble-pops-1.jpg
  - /images/groovy/marble-pops-2.jpg
  - /images/groovy/marble-pops-3.jpg
  - /images/groovy/marble-pops-4.jpg
  - /images/groovy/marble-pops-5.jpg
sku: HJ-GR-MARBLE-POPS
badge: ''
berat: 700 gram
color:
  - Marble Pops
size:
  - name: All Size
    price: 185000
  - name: XL
    price: 195000
stock: true
---

HIJACKET GROOVY ORIGINAL yang dirancang Hi-Fashion dengan tampilan super modis. Seri GROOVY adalah seri EKSTRIM, yang memiliki 2 sisi berbeda. Semakin ekstrim, semakin diminati oleh Hijaber

- ▶️ Ukuran : ALL SIZE FIT TO L hingga XL (XL Nambah 10.000)

- ▶️ Material : Premium Fleece yang “SOFT TOUCH” langsung dari pabrik pengolah kain berpengalaman

- ▶️ Proses : Dibuat Handmade dengan penjahit terbaik yang berpengalaman lebih dari 5 tahun

- ▶️ Printing Dada : Polyflex Berkualitas untuk Icon Brand Hijacket

- ▶️ Bukan sekedar fashion. Namun menguatkan “JATI DIRI / IDENTITAS” Hijaber yang modis dan stylish

- ▶️ Foto & Video : 100% sama dengan hijacket yang diterima karena kami foto & video model sendiri.

Ada 3 variasi warna Hijacket Grovy Original

#### Tabel Ukuran Hijacket Groovy Original


| Ukuran          | All Size        | XL              |
|:--------------- |:---------------:|:---------------:|
| Lingkar Dada    | 101-102         | 108-110	      |
| Lingkar Lengan  | 40-42           | 43-45  	      |
| Panjang Tangan  | 55-57           | 55-57  	      |
| Panjang Badan   | 93-95           | 93-95  	      |
